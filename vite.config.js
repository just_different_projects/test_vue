import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { fileURLToPath, URL } from 'node:url'

const createAlias = (path) => fileURLToPath(new URL(path, import.meta.url))

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@assets': createAlias('./src/assets'),
      '@utils': createAlias('./src/utils'),
      '@stores': createAlias('./src/stores'),
      '@components': createAlias('./src/components'),
      '@pages': createAlias('./src/pages'),
      '@layouts': createAlias('./src/layouts'),
      '@common': createAlias('./src/common')
    }
  }
})

import axios from 'axios'
import { useAlertStore } from '@stores/alert'

export const baseApi = axios.create({
  baseURL: import.meta.env.VITE_API_BASE_URL + import.meta.env.VITE_API_STORE_ID
})

baseApi.interceptors.request.use(async (config) => {
  config.headers.Authorization = 'Bearer ' + import.meta.env.VITE_API_TOKEN
  return config
})

baseApi.interceptors.response.use(
  async (response) => response,
  async (error) => {
    const store = useAlertStore()

    if (error.response?.status === 401 || error.response?.status === 403) {
      store.showAlert({
        title: 'Ошибка',
        message: 'Ошибка авторизации'
      })
    }
    if (error.response?.status === 500) {
      store.showAlert({
        title: 'Ошибка',
        message: 'Внутренняя ошибка сервера. Обратитесь к администратору'
      })
    }
    if (error.code === 'ERR_NETWORK') {
      store.showAlert({
        title: 'Сетевая ошибка',
        message: 'Превышено время ожидания сервера. Попробуйте повторить запрос'
      })
    }
    return Promise.reject(error)
  }
)

export interface pagingOptions {
  total: number
  count: number
  offset: number
  limit: number
}

export interface loadableState {
  isLoaded: boolean
}

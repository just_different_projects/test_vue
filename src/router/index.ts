import { createRouter, createWebHistory } from 'vue-router'

import ProductsPage from '@pages/Products.vue'
import ProductPage from '@pages/Product.vue'
import CartPage from '@pages/Cart.vue'
import NotFoundPage from '@pages/NotFound.vue'

const routes = [
  { path: '/', name: 'main', component: ProductsPage },
  { path: '/category/:slug?', name: 'category', component: ProductsPage },
  { path: '/product/:id', name: 'product', component: ProductPage },
  { path: '/cart', name: 'cart', component: CartPage },
  {
    path: '/404',
    name: 'NotFound',
    component: NotFoundPage
  },
  { path: '/:pathMatch(.*)*', redirect: '404' }
]

export default createRouter({
  history: createWebHistory(),
  routes
})
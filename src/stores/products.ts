import { defineStore } from 'pinia'
import { baseApi } from '@utils/api'
import { loadableState, pagingOptions } from '@common/types'

export interface productListItem {
  id: number
  name: string
  price: number
  thumbnail: string
}

export interface productItem extends productListItem {
  description: string
  image: string
}

interface productsState extends loadableState {
  products: productListItem[]
  productsPaging: pagingOptions
  currentProduct: productItem
}

export const useProductsStore = defineStore('productsStore', {
  state: (): productsState => ({
    isLoaded: false,
    products: [],
    currentProduct: {
      id: 0,
      name: '',
      price: 0,
      thumbnail: '',
      description: '',
      image: ''
    },
    productsPaging: {
      total: 0,
      count: 0,
      offset: 0,
      limit: 0
    }
  }),
  actions: {
    async loadProducts(categoryId: number = 0) {
      this.isLoaded = false
      const response = await baseApi.get('/products', {
        params: {
          categories: categoryId
        }
      })

      this.productsPaging = {
        total: response.data.total,
        count: response.data.count,
        offset: response.data.offset,
        limit: response.data.limit
      }

      this.products = response.data.items.map((item: any) => {
        return {
          id: item.id,
          name: item.name,
          description: item.description,
          image: item.hdThumbnailUrl,
          thumbnail: item.thumbnailUrl,
          price: item.price
        }
      })
      this.isLoaded = true
    },
    async loadProduct(id: string) {
      this.isLoaded = false
      const response = await baseApi.get('/products/' + id)

      this.currentProduct = {
        id: response.data.id,
        name: response.data.name,
        description: response.data.description,
        image: response.data.hdThumbnailUrl,
        thumbnail: response.data.thumbnailUrl,
        price: response.data.price
      }

      this.isLoaded = true
    }
  },
})

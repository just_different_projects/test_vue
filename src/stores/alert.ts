import { defineStore } from 'pinia'

interface showState {
  show: boolean
}

interface alertState {
  message: string
  title: string
}

export const useAlertStore = defineStore('alertStore', {
  state: (): showState & alertState => {
    return {
      show: false,
      message: '',
      title: ''
    }
  },
  actions: {
    showAlert({ title, message }: alertState) {
      this.show = true
      this.title = title
      this.message = message
    },
    closeAlert() {
      this.show = false
      this.title = ''
      this.message = ''
    }
  }
})

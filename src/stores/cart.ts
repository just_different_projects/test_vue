import { defineStore } from 'pinia'
import { productListItem } from '@stores/products'
import { useLocalStorage } from '@vueuse/core'
import { RemovableRef } from '@vueuse/shared'

const CART_STORE_NAME = 'cartStore'

export interface cartItem {
  count: number
  item: productListItem
}

export interface cartTotalItem {
  total: number
  item: productListItem
}

interface cartState {
  products: RemovableRef<Map<number, cartItem>>
}

export const useCartStore = defineStore(CART_STORE_NAME, {
  state: (): cartState => ({
    products: useLocalStorage(CART_STORE_NAME, new Map())
  }),
  getters: {
    count(): number {
      let count = 0
      this.products.forEach(value => count += value.count)
      return count
    },
    getTotalItems(): cartTotalItem[] {
      let totalItems: cartTotalItem[] = []
      this.products.forEach(value => {
        totalItems.push({
          total: value.item.price * value.count,
          item: value.item
        })
      })
      return totalItems
    },
    getTotalPrice(): number {
      return this.getTotalItems.reduce((sum, item) => sum + item.total, 0)
    }
  },
  actions: {
    increaseProduct(product: productListItem) {
      if (this.products.has(product.id)) {
        this.products.get(product.id)!.count++
      } else {
        this.products.set(product.id, {
          count: 1,
          item: product
        })
      }
    },
    decreaseProduct(product: productListItem) {
      if (this.products.has(product.id)) {
        this.products.get(product.id)!.count--
        if (this.products.get(product.id)!.count < 1) {
          this.removeProduct(product)
        }
      }
    },
    removeProduct(product: productListItem) {
      this.products.delete(product.id)
    },
    getProduct(id: number) {
      return this.products.get(id)
    },
    clearCart() {
      this.products.clear()
    }
  }
})

interface ImportMetaEnv {
  readonly VITE_API_BASE_URL: string
  readonly VITE_API_STORE_ID: number
  readonly VITE_API_TOKEN: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
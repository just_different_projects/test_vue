## Project Setup

```sh
npm install
```

### Development

```sh
npm run dev
```

### Production

```sh
npm run build
```
